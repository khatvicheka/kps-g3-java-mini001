public class Stock {
    private int id;
    private String bookName;
    private double unitPrice;
    private int stockQuantity;
    public String importedDate;

    public Stock(int id, String bookName, double unitPrice, int stockQuantity, String importedDate) {
        this.id = id;
        this.bookName = bookName;
        this.unitPrice = unitPrice;
        this.stockQuantity = stockQuantity;
        this.importedDate = importedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getImportedDate() {
        return importedDate;
    }

    public void setImportedDate(String importedDate) {
        this.importedDate = importedDate;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                ", unitPrice=" + unitPrice +
                ", stockQuantity=" + stockQuantity +
                ", importedDate='" + importedDate + '\'' +
                '}';
    }
}
