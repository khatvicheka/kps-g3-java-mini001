import java.sql.*;
public class dbConnection {
    public static Connection connectDB() {
        Connection c = null;

        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DBStock",
                    "postgres", "postgres001");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
//            System.out.println("u");
        }
        return c;
    }

    public static void closeConnection(ResultSet rs, PreparedStatement preparedStatement, Connection c){
        try {
            rs.close();
            preparedStatement.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
//            System.out.println("close");
        }

    }
}
