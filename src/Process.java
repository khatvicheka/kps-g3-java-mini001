
import org.apache.log4j.*;
import org.nocrala.tools.texttablefmt.*;
import java.sql.*;
import java.util.Scanner;
public class Process {
    static Connection c = null;
    static PreparedStatement preparedStatement = null;
    Scanner scanner = new Scanner(System.in);

    //Loading method
    public void Loading(String m, long l){
        for (int i=0;i<m.length();i++){
            System.out.print(m.charAt(i));
            try {
                Thread.sleep(l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
        System.out.println("Current time loading : 3");
    }
    //Display menu text
    public void displayMenu(){
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left,
                CellStyle.AbbreviationStyle.crop,CellStyle.NullStyle.emptyString);
        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL,false,"");
        Logger.getRootLogger().setLevel(Level.OFF);
        String st = "*)Display W)rite R)ead U)pdate D)elete F)irst P)revious N)ext L)ast" +
                " S)earch G)oto Se)et row Sa)ave B)ack upN R)estore H)elp E)xit";
        t.addCell(st,cs);
        System.out.println(t.render());
    }

    //Choose option
    public void Menu(){
        c = dbConnection.connectDB();
        String option;
        while (true){
            displayMenu();
            System.out.print("Command --> ");
            option = scanner.next();
            switch (option){
                case "*" :
                    //Display data
                    displayData();
                    break;
                case "w" : break;
                case "r" : break;
                case "u" : break;
                case "d" : break;
                case "f" : break;
                case "p" : break;
                case "n" : break;
                case "l" : break;
                case "s" : break;
                case "g" : break;
                case "se" : break;
                case "sa" : break;
                case "b" : break;
                case "re" : break;
                case "h" : break;
                case "e" :
                    System.exit(0);
                    break;
            }
        }
    }

    //TODO: display data
    public void displayData(){
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left,
                CellStyle.AbbreviationStyle.crop,CellStyle.NullStyle.emptyString);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL,false,"");
        Logger.getRootLogger().setLevel(Level.OFF);

        t.addCell("ID",cs);
        t.addCell("NAME",cs);
        t.addCell("UNIT PRICE",cs);
        t.addCell("QUANTITY",cs);
        t.addCell("IMPORTED DATE",cs);

        try {
            preparedStatement = c.prepareStatement("SELECT * FROM book");
            ResultSet rs = preparedStatement.executeQuery(); //use executeQuery() because we are just fetching the data

            while (rs.next()) { // read data row by row, and next() method return true if the resultSet still has next row of data
                int bookId = rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                String unitPrice = rs.getString("unit_price");
                int quantity = rs.getInt("stock_quantity");
                String importedDate = rs.getString("imported_date");

                t.addCell(String.valueOf(bookId),cs);
                t.addCell(bookName,cs);
                t.addCell(unitPrice,cs);
                t.addCell(String.valueOf(quantity),cs);
                t.addCell(importedDate,cs);
            }
            System.out.println(t.render());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //Read data
    public void readData(){
        c = dbConnection.connectDB();

    }
    //Search Data
    public void searchData(){

    }
}
